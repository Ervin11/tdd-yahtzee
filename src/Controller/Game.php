<?php

namespace App\Controller;

class Game
{
    public function calculateOnes(array $dices): int
    {
        return count(array_filter($dices, function($number) { return $number === 1; }));
    }

    public function calculateTwos(array $dices): int
    {
        return 2 * count(array_filter($dices, function($number) { return $number === 2; }));
    }

    public function calculateThrees(array $dices): int
    {
        return 3 * count(array_filter($dices, function($number) { return $number === 3; }));
    }

    public function calculateFours(array $dices): int
    {
        return 4 * count(array_filter($dices, function($number) { return $number === 4; }));
    }

    public function calculateFives(array $dices): int
    {
        return 5 * count(array_filter($dices, function($number) { return $number === 5; }));
    }

    public function calculateSixes(array $dices): int
    {
        return 6 * count(array_filter($dices, function($number) { return $number === 6; }));
    }

    public function calculateThreeKind(array $dices): int
    {
        for ($i = 1; $i < 6; $i++) {
            if (count(array_filter($dices, function($number) use ($i) { return $number === $i; })) >= 3) {
                return array_sum($dices);
            }
        }

        return 0;
    }

    public function calculateFourKind(array $dices): int
    {
        for ($i = 1; $i < 6; $i++) {
            if (count(array_filter($dices, function($number) use ($i) { return $number === $i; })) >= 4) {
                return array_sum($dices);
            }
        }

        return 0;
    }

    public function calculateFullHouse(array $dices): int
    {
        $formattedDices = array_count_values($dices);
        $isThreeKind = array_search(3, $formattedDices);

        if ($isThreeKind) {

            $isNotThreeKind = array_filter($dices, function($number) use ($isThreeKind) { return $number !== $isThreeKind; });

            if ($isNotThreeKind[array_key_first($isNotThreeKind)] === $isNotThreeKind[array_key_last($isNotThreeKind)]) {
                return 25;
            }
        }

        return 0;
    }

    public function calculateSmallStraight(array $dices): int
    {
        if (in_array(1, $dices) AND in_array(2, $dices) AND in_array(3, $dices) AND in_array(4, $dices)) {
            return 30;
        }

        if (in_array(2, $dices) AND in_array(3, $dices) AND in_array(4, $dices) AND in_array(5, $dices)) {
            return 30;
        }

        if (in_array(3, $dices) AND in_array(4, $dices) AND in_array(5, $dices) AND in_array(6, $dices)) {
            return 30;
        }

        return 0;
    }

    public function calculateLargeStraight(array $dices): int
    {
        if (in_array(1, $dices) AND in_array(2, $dices) AND in_array(3, $dices) AND in_array(4, $dices) AND in_array(5, $dices)) {
            return 40;
        }

        if (in_array(2, $dices) AND in_array(3, $dices) AND in_array(4, $dices) AND in_array(5, $dices) AND in_array(6, $dices)) {
            return 40;
        }

        return 0;
    }

    public function calculateChance(array $dices): int
    {
        return array_sum($dices);
    }

    public function calculateYahtzee(array $dices): int
    {
        $formattedDices = array_count_values($dices);
        $isYahtzee = array_search(5, $formattedDices);

        return $isYahtzee ? 50 : 0;
    }
}
