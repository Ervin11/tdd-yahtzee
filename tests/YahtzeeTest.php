<?php

use App\Controller\Game;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class YahtzeeTest extends KernelTestCase
{

    private $game;

    protected function setUp(): void
   {
       parent::setUp();

       $this->game = new Game();
   }


    /**
     * @testWith [[1, 2, 3, 4, 5], 1]
     *           [[1, 1, 3, 4, 5], 2]
     *           [[2, 2, 3, 4, 5], 0]
     */
    public function testOnes(array $dices, int $expected)
    {
        $result = $this->game->calculateOnes($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 2, 3, 4, 5], 2]
     *           [[1, 1, 3, 4, 5], 0]
     *           [[2, 2, 3, 4, 5], 4]
     */
    public function testTwos(array $dices, int $expected)
    {
        $result = $this->game->calculateTwos($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 2, 3, 4, 5], 3]
     *           [[1, 1, 1, 4, 5], 0]
     *           [[2, 3, 3, 4, 5], 6]
     */
    public function testThrees(array $dices, int $expected)
    {
        $result = $this->game->calculateThrees($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 2, 3, 4, 5], 4]
     *           [[1, 1, 4, 4, 5], 8]
     *           [[2, 2, 3, 3, 5], 0]
     */
    public function testFours(array $dices, int $expected)
    {
        $result = $this->game->calculateFours($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 2, 3, 4, 5], 5]
     *           [[1, 1, 3, 5, 5], 10]
     *           [[2, 2, 3, 4, 4], 0]
     */
    public function testFives(array $dices, int $expected)
    {
        $result = $this->game->calculateFives($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 2, 3, 4, 5], 0]
     *           [[1, 1, 3, 4, 6], 6]
     *           [[2, 2, 3, 6, 6], 12]
     */
    public function testSixes(array $dices, int $expected)
    {
        $result = $this->game->calculateSixes($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 2, 3, 4, 5], 0]
     *           [[1, 1, 3, 4, 5], 0]
     *           [[1, 1, 1, 4, 5], 12]
     *           [[1, 1, 1, 1, 1], 5]
     */
    public function testThreeKind(array $dices, int $expected)
    {
        $result = $this->game->calculateThreeKind($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 2, 3, 4, 5], 0]
     *           [[1, 1, 3, 4, 5], 0]
     *           [[1, 1, 1, 1, 5], 9]
     *           [[1, 1, 1, 1, 1], 5]
     */
    public function testFourKind(array $dices, int $expected)
    {
        $result = $this->game->calculateFourKind($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 2, 3, 4, 5], 0]
     *           [[1, 1, 3, 4, 5], 0]
     *           [[1, 1, 1, 2, 2], 25]
     *           [[1, 1, 1, 1, 2], 0]
     */
    public function testFullHouse(array $dices, int $expected)
    {
        $result = $this->game->calculateFullHouse($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 1, 2, 4, 5], 0]
     *           [[1, 1, 3, 2, 5], 0]
     *           [[1, 2, 3, 4, 2], 30]
     */
    public function testSmallStraight(array $dices, int $expected)
    {
        $result = $this->game->calculateSmallStraight($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 1, 2, 4, 5], 0]
     *           [[1, 1, 3, 2, 5], 0]
     *           [[1, 2, 3, 4, 5], 40]
     */
    public function testLargeStraight(array $dices, int $expected)
    {
        $result = $this->game->calculateLargeStraight($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 1, 1, 1, 1], 5]
     *           [[1, 1, 1, 5, 5], 13]
     *           [[1, 2, 3, 4, 5], 15]
     */
    public function testChance(array $dices, int $expected)
    {
        $result = $this->game->calculateChance($dices);
        $this->assertEquals($expected, $result);
    }

    /**
     * @testWith [[1, 2, 3, 4, 5], 0]
     *           [[1, 1, 1, 5, 5], 0]
     *           [[1, 1, 1, 1, 1], 50]
     */
    public function testYahtzee(array $dices, int $expected)
    {
        $result = $this->game->calculateYahtzee($dices);
        $this->assertEquals($expected, $result);
    }

}
